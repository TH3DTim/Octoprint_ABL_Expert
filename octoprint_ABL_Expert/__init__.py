from __future__ import absolute_import

import json
import threading
import flask
import requests
import octoprint.plugin  # pylint: disable=E0401
from octoprint.events import Events  # pylint: disable=E0401

class ABL_VersionCheck:
    @classmethod
    def get_latest(cls, target, check, full_data=False, online=True):
        current_version = check.get("current", "1.0.0")
        remote_version = current_version
        if online:
            r = requests.get('https://framagit.org/razer/Octoprint_ABL_Expert'
                             + '/raw/master/VERSION?inline=false')
            if r.status_code == 200:
                remote_version = r.text.strip().encode('ascii', 'ignore')

        info = dict(local=dict(name=current_version, value=current_version),
                    remote=dict(name=remote_version, value=remote_version))

        return info, True

# pylint: disable=R0901,R0902
class ABL_ExpertPlugin(octoprint.plugin.AssetPlugin,
                       octoprint.plugin.EventHandlerPlugin,
                       octoprint.plugin.SimpleApiPlugin,
                       octoprint.plugin.SettingsPlugin,
                       # octoprint.plugin.StartupPlugin,
                       octoprint.plugin.TemplatePlugin):

    # pylint: disable=W0201,W0231
    def initialize(self):
        self.state = 'idle'
        self.on_eeprom_fetch = False
        self.on_bed_leveling = False
        self.preheat_done = False
        self.on_mesh_log = 0
        self.eeprom_mesh = []
        self.probe_mesh = []
        self.fixed_mesh = []
        self.grid_size = None
        self.profile = self._printer_profile_manager.get_current_or_default()
        self.printer_cap = {'eeprom': None, 'z_probe': None,
                            'autolevel': None, 'leveling_data': None}
        self.simulate_count = 0

    def get_update_information(self):
        return dict(ABL_Expert=dict(
            displayName='ABL Expert Plugin',
            displayVersion=self._plugin_version,
            current=self._plugin_version,
            type='python_checker',
            python_checker=ABL_VersionCheck,
            pip=('https://framagit.org/razer/Octoprint_ABL_Expert/-/archive' +
                 '/master/Octoprint_ABL_Expert-master.zip')))

    @classmethod
    def get_assets(cls):
        return dict(js=['ABLexpert.js'], less=['ABLexpert.less'])

    def get_template_vars(self):
        return dict(profile=self.profile,
                    printer_cap=self.printer_cap)

    @classmethod
    def get_settings_defaults(cls):
        return dict(preheat_selected='Disabled', preheat_bed_only=False,
                    probe_offset=0, x_bilinear=False, y_bilinear=False,
                    cooldown=True, x_point_index=0, x_point_offset=0,
                    y_point_index=0, y_point_offset=0, x_final_offset=0,
                    y_final_offset=0, eeprom_save=False, max_index=1)

    def on_settings_save(self, data):
        s = self._settings
        old_z_offset = s.get_float(['probe_offset'])
        octoprint.plugin.SettingsPlugin.on_settings_save(self, data)
        new_z_offset = s.get_float(['probe_offset'])
        if new_z_offset < 0:
            # Force value to be positive, sign is added in gcode
            self._settings.set(['probe_offset'], abs(new_z_offset))
            new_z_offset = self._settings.get_float(['probe_offset'])
        if old_z_offset != new_z_offset:
            printer_commands = ['M851Z-{}'.format(new_z_offset)]
            if s.get_boolean(['eeprom_save']) and self.printer_cap['eeprom']:
                printer_commands.append('M500')
            self._printer.commands(printer_commands)
        self._logger.info('Offset fix enabled: %s', self.offset_fix_enabled)

    @classmethod
    def get_api_commands(cls):
        return dict(bed_leveling=[], correction_preview=['args'], cancel=[])

    def on_api_command(self, command, data):
        if command == 'bed_leveling':
            # self.simulate_process()
            self.do_leveling()
        if command == 'correction_preview':
            if not self.grid_size:
                return
            # Generate plate mesh
            mesh = []
            for _ in range(self.grid_size):
                line = []
                for _ in range(self.grid_size):
                    line.append(0.0)
                mesh.append(line)
            self._logger.debug('Preview with data %s', data)
            s = {'x_final_offset': float(data['args'][0])}
            x_bilinear = data['args'][1]
            s['x_point_index'] = int(data['args'][2]) if x_bilinear else 0
            s['x_point_offset'] = float(data['args'][3]) if x_bilinear else 0.0
            s['y_final_offset'] = float(data['args'][4])
            y_bilinear = data['args'][5]
            s['y_point_index'] = int(data['args'][6]) if y_bilinear else 0
            s['y_point_offset'] = float(data['args'][7]) if y_bilinear else 0.0
            fixed_mesh = self.bed_leveling_fix(mesh, dry_run=s)
            self._send_message('probed_mesh', json.dumps(mesh))
            self._send_message('fixed_mesh', json.dumps(fixed_mesh))
        if command == 'cancel':
            self.preheat_done = False
            self.on_bed_leveling = False
            self.on_mesh_log = 0
            if 'probing' in self.state and self._printer.is_operational():
                self._printer.disconnect()
                self._printer.connect()
            self.set_state('idle')

    def on_api_get(self, unused_request):
        printer_state = self._printer.get_current_connection()[0].lower()
        printer_online = printer_state == 'operational'
        return flask.jsonify(eeprom_mesh=self.eeprom_mesh,
                             printer_cap=self.printer_cap,
                             printer_online=printer_online)

    def set_state(self, state):
        self.state = state
        self._send_message('state', state)

    @property
    def offset_fix_enabled(self):
        s = self._settings
        return bool(s.get_float(['x_final_offset'])
                    or s.get_float(['y_final_offset'])
                    or s.get_boolean(['x_bilinear'])
                    or s.get_boolean(['y_bilinear']))

    def on_event(self, event, payload):
        if event == 'Disconnected':
            self.on_eeprom_fetch = False
            self.on_bed_leveling = False
            self.preheat_done = False
            self.on_mesh_log = 0
            self.set_state('idle')
            self._send_message('printer_online', False)
            self.printer_cap = {'eeprom': None, 'z_probe': None,
                                'autolevel': None, 'leveling_data': None}
            self._send_message('printer_cap', json.dumps(self.printer_cap))
        if event == 'Connected':
            self._send_message('printer_online', True)
            self._send_message('printer_cap', json.dumps(self.printer_cap))
        if event == Events.FIRMWARE_DATA:
            self._logger.debug('Get firmware data: %s - %s',
                              payload.get('name'), payload.get('data'))

    def preheat_wait(self):
        if not self.on_bed_leveling:
            return
        temp_map = self._printer.get_current_temperatures()
        bed_ready = True
        tool_ready = True
        if self.profile['heatedBed']:
            if temp_map['bed']['actual'] + 3 < temp_map['bed']['target']:
                bed_ready = False
        if temp_map['tool0']['actual'] + 3 < temp_map['tool0']['target']:
            tool_ready = False
        if bed_ready and tool_ready:
            self._logger.info('Preheat is done')
            self.preheat_done = True
            self.set_state('probing')
            self._printer.commands(['G28', 'G29'])
            return
        self._logger.debug('Preheating...')
        threading.Timer(3.0, self.preheat_wait).start()

    # pylint: disable=W0613,R0913,W0631,W1113
    # def on_printer_gcode_queued(self, comm_instance, phase, cmd, cmd_type,
    #                             gcode, subcode=None, tags=None,
    #                             *args, **kwargs):
    #     s = self._settings
    #     if not s.get_boolean(['g29_process']):
    #         return None
    #     if gcode != 'G29':
    #         return None
    #     # self._logger.info('G29 (mesh leveling) command detected')
    #     return self.do_leveling()

    def do_leveling(self):
        s = self._settings
        if self.on_bed_leveling:
            if not self.preheat_done:
                self._logger.warning('Already in leveling procedure !')
                return
            if 'Disabled' in s.get(['preheat_selected']) or self.preheat_done:
                self.preheat_done = True
                self.set_state('probing')
                return

        self.on_bed_leveling = 1
        for temp_profile in s.global_get(['temperature', 'profiles']):
            if temp_profile['name'] in s.get(['preheat_selected']):
                break
        if not temp_profile:
            return
        self.set_state('preheating')
        if self.profile['heatedBed']:
            self._printer.set_temperature('bed', temp_profile['bed'])
        # pylint: disable=C0330
        if (not self.profile['heatedBed'] or (self.profile['heatedBed']
            and not s.get_boolean(['preheat_bed_only']))):
            self._printer.set_temperature('tool0',
                                          temp_profile['extruder'])
        threading.Timer(3, self.preheat_wait).start()
        return

    # pylint: disable=W0613,R0911,R0912,R0915
    def on_printer_gcode_received(self, comm, line, *args, **kwargs):
        if len(line) < 3:
            return line
        line_lower = line.lower()
        if 'cap:' in line_lower:
            # Printer capabilities
            for cap in self.printer_cap:
                if cap in line_lower:
                    self.printer_cap[cap] = int(line.split(':')[-1])
                    self._logger.info('self.printer_cap[%s] is now %s',
                                      cap, line.split(':')[-1])
                    if cap in 'eeprom' and self.printer_cap[cap]:
                        self.on_eeprom_fetch = True
                        self._printer.commands(['M420V1', 'M851'])
                    break
            self._send_message('printer_cap', json.dumps(self.printer_cap))
            return line
        if 'probe z offset:' in line_lower:
            self._settings.set(['probe_offset'],
                               abs(float(line.split(':')[-1])))
            return line
        # if self.on_bed_leveling and 'home xyz first' in line_lower:
        #     self._logger.info('Printer reports unhomed axis')
        #     self._printer.commands(['G28', 'G29'])
        #     return line
        if 'leveling grid:' in line_lower:
            # Begining of leveling grid info sequence
            self._logger.debug('Leveling grid sequence begins')
            self.on_mesh_log = 1
            if self.on_bed_leveling:
                self.set_state('wait_result')
                self.probe_mesh = []
            elif self.on_eeprom_fetch:
                self.eeprom_mesh = []
            return line
        if self.on_mesh_log > 1 and self.on_mesh_log > self.grid_size + 1:
            # End of leveling grid info sequence
            self._logger.debug('Leveling grid sequence ends')
            self.on_mesh_log = 0
            self.preheat_done = False
            if self._settings.get_boolean(['cooldown']):
                self._printer.set_temperature('bed', 0)
                self._printer.set_temperature('tool0', 0)
            if self.on_eeprom_fetch:
                # Mesh comes from printer's eeprom
                self.on_eeprom_fetch = False
                self._logger.debug('Eeprom mesh:')
                for row in self.eeprom_mesh:
                    self._logger.debug(row)
                self._send_message('eeprom_mesh', json.dumps(self.eeprom_mesh))
                return line
            if self.on_bed_leveling:
                # Mesh comes from leveling process
                self.on_bed_leveling = False
                if self.offset_fix_enabled:
                    self.set_state('mesh_fix')
                    # self.fixed_mesh = self.bed_leveling_fix(TEST_MESH)
                    self.fixed_mesh = self.bed_leveling_fix(self.probe_mesh)
            self._logger.debug('Probed mesh:')
            for row in self.probe_mesh:
                self._logger.debug(row)
            self._send_message('probed_mesh', json.dumps(self.probe_mesh))
            if self.offset_fix_enabled:
                self._send_message('fixed_mesh', json.dumps(self.fixed_mesh))
            self.set_state('idle')
            return line

        if not self.on_mesh_log:
            # if 'FIRMWARE_NAME' in line
            return line
        if self.on_mesh_log == 1:
            # First line: X indexes
            self.grid_size = len(line.strip().split())
            self._settings.set(['max_index'], self.grid_size - 1)
            self._logger.info('Grid mesh size is %s', self.grid_size)
        else:
            # self._logger.info('Leveling grid line: %s', line)
            grid_line = line.strip().split()
            grid_line.pop(0)
            if self.on_bed_leveling:
                self.probe_mesh.append(grid_line)
            elif self.on_eeprom_fetch:
                self.eeprom_mesh.append(grid_line)
        self.on_mesh_log += 1
        return line

    # pylint: disable=R0914
    def bed_leveling_fix(self, mesh, dry_run=False):
        """ Return software correction on leveling_result mesh
            level_result : output raw of G29 or M420V1 gcode command
        """
        values = dry_run
        if not values:
            s = self._settings
            values = {'x_final_offset': s.get_float(['x_final_offset']),
                      'x_point_index': 0, 'x_point_offset': 0.0,
                      'y_final_offset': s.get_float(['y_final_offset']),
                      'y_point_index': 0, 'y_point_offset': 0.0}
            if s.get_boolean(['x_bilinear']):
                values['x_point_index'] = s.get_int(['x_point_index'])
                values['x_point_offset'] = s.get_float(['x_point_offset'])
            if s.get_boolean(['y_bilinear']):
                values['y_point_index'] = s.get_int(['y_point_index'])
                values['y_point_offset'] = s.get_float(['y_point_offset'])
        corrected_level_result = []
        first_iteration = True
        y_index = 0

        def _get_offset(index, initial_offset, point_index, point_offset):
            if index > point_index:
                return point_offset + (initial_offset * (
                    float(index) - point_index))
            if point_index and index < point_index:
                return (float(point_offset) / point_index) * float(index)
            return point_offset

        for row in mesh:
            if first_iteration:
                first_iteration = False
                grid_size = len(row) - 1
                diviser_map = {
                    'x': float(grid_size) - values['x_point_index'],
                    'y': float(grid_size) - values['y_point_index']}
                for _, diviser in diviser_map.items():
                    if diviser == 0:
                        self._logger.error('Preventing division by zero, ' +
                                           'check your point indexes !')
                        diviser = 1
                x_offset = (values['x_final_offset'] -
                            values['x_point_offset']) / diviser_map['x']
                y_offset = (values['y_final_offset'] -
                            values['y_point_offset']) / diviser_map['y']
                self._logger.debug(
                    'Grid size:{} - X diviser:{} - Y diviser:{}'.format(
                        grid_size, round(diviser_map['x'], 3),
                        round(diviser_map['y'], 3)))
                self._logger.debug('X offset:{} - Y offset:{}'.format(
                    round(x_offset, 3), round(y_offset, 3)))

            y_new_offset = 0
            y_new_offset = _get_offset(y_index, y_offset,
                                       values['y_point_index'],
                                       values['y_point_offset'])
            x_count = 0
            corrected_row = []
            for z_initial in row:
                x_new_offset = _get_offset(x_count, x_offset,
                                           values['x_point_index'],
                                           values['x_point_offset'])
                point_offset = x_new_offset + y_new_offset
                z_value = str(round(float(z_initial) + point_offset, 3))
                corrected_row.append(z_value)
                x_count += 1
            corrected_level_result.append(corrected_row)
            y_index += 1
        if not dry_run:
            self.store_mesh(corrected_level_result)
        return corrected_level_result

    def store_mesh(self, mesh):
        """ Send corrected mesh to printer via gcode M421 commands
        """
        s = self._settings
        printer_commands = []
        index_y = 0
        for row in mesh:
            index_x = 0
            for z_offset in row:
                printer_commands.append('M421I{}J{}Z{}'.format(
                    index_x, index_y, z_offset))
                index_x += 1
            index_y += 1
        if s.get_boolean(['eeprom_save']) and self.printer_cap['eeprom']:
            printer_commands.append('M500')
        self._logger.debug('Store mesh commands: %s', printer_commands)
        self._printer.commands(printer_commands)

    def _send_message(self, msg_type, message):
        self._plugin_manager.send_plugin_message(
            self._identifier,
            dict(type=msg_type,
                 msg=message))

    def simulate_process(self):
        if not self.simulate_count:
            self.set_state('preheating')
        elif self.simulate_count == 1:
            self.set_state('probing')
        elif self.simulate_count == 2:
            self.set_state('wait_result')
        elif self.simulate_count == 3:
            self.set_state('idle')
            self._send_message('probed_mesh', json.dumps(self.eeprom_mesh))
            self._send_message('fixed_mesh', json.dumps(self.eeprom_mesh))
            self.simulate_count = 0
            return
        self.simulate_count += 1
        threading.Timer(5.0, self.simulate_process).start()

__plugin_name__ = 'ABL Expert Plugin'


def __plugin_load__():
    # pylint: disable=W0601
    global __plugin_implementation__
    __plugin_implementation__ = ABL_ExpertPlugin()

    global __plugin_hooks__
    __plugin_hooks__ = {
        'octoprint.plugin.softwareupdate.check_config':
        __plugin_implementation__.get_update_information,
        'octoprint.comm.protocol.gcode.received':
        __plugin_implementation__.on_printer_gcode_received,
        # 'octoprint.comm.protocol.gcode.queuing':
        # __plugin_implementation__.on_printer_gcode_queued
    }
