'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AblExpertViewModel = function () {
  function AblExpertViewModel(parameters) {
    var _this = this;

    _classCallCheck(this, AblExpertViewModel);

    this.settingsViewModel = parameters[0];
    this.control = parameters[1];
    this.temp = parameters[2];
    this.preheatOptions = ko.observableArray(undefined);
    this.printerOnline = ko.observable(undefined);
    this.printerEeprom = ko.observable(undefined);
    this.printerAbl = ko.observable(undefined);
    this.printerZprobe = ko.observable(undefined);
    this.printerLackCap = ko.observable(undefined);
    this.preheatingEnabled = ko.observable(false);
    this.preheatingDone = ko.observable(undefined);
    this.eepromMeshTable = ko.observable(undefined);
    this.probedMeshTable = ko.observable(undefined);
    this.fixedMeshTable = ko.observable(undefined);
    this.onLeveling = ko.observable(undefined);
    this.ruler = ko.observable(undefined);
    this.tempToolVisible = ko.observable(false);
    this.probedMesh = [];
    this.state = 'idle';
    this.processingDialog = $('#processing_dialog_plugin_ABL_Expert');
    this.processingDialog.modal({ keyboard: false, backdrop: 'static', show: false });
    this.processingDialog.on('hidden.bs.modal', function () {
      _this.onProcessingDialogClosed();
    });
    this.processingDialogVisible = false;
    this.resultsDialog = $('#results_dialog_plugin_ABL_Expert');
    this.resultsDialog.modal({ keyboard: false, backdrop: 'static', show: false });
    this.resultsDialog.on('hidden.bs.modal', function () {
      _this.onResultsDialogClosed();
    });
    this.resultsDialogVisible = false;
    // Add bed leveling button in control tab with callback
    $("#control-jog-general").append($('#controlAblButton').html());
    this.control.doLeveling = function () {
      _this.doLeveling();
    };
  }

  _createClass(AblExpertViewModel, [{
    key: 'onBeforeBinding',
    value: function onBeforeBinding() {
      var _this2 = this;

      this.pluginSettings = this.settingsViewModel.settings.plugins.ABL_Expert;
      this.preheatOptions.push('Disabled');
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.settingsViewModel.settings.temperature.profiles()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var profile = _step.value;

          this.preheatOptions.push(profile.name());
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      this.preheatingEnabled(this.pluginSettings.preheat_selected() != 'Disabled');
      this.preheatingDone(this.state != 'preheating');
      this.onLeveling(this.state != 'idle');
      this.ruler('');
      this.request('GET', null, null, function (data) {
        _this2.updatePrinterCap(data.printer_cap);
        _this2.eepromMeshTable(_this2.getMeshTable(data.eeprom_mesh));
        _this2.printerOnline(data.printer_online);
      });
    }
  }, {
    key: 'onEventSettingsUpdated',
    value: function onEventSettingsUpdated(payload) {
      this.onBeforeBinding();
    }
  }, {
    key: 'onAfterBinding',
    value: function onAfterBinding() {
      var _this3 = this;

      setInterval(function () {
        if (_this3.processingDialogVisible && _this3.state != 'idle') {
          if (_this3.ruler().length >= 3) _this3.ruler('');
          _this3.ruler(_this3.ruler() + '.');
          return;
        }
        if (_this3.state != 'idle') _this3.openProcessingDialog();
      }, 1000);
    }
  }, {
    key: 'onDataUpdaterPluginMessage',
    value: function onDataUpdaterPluginMessage(plugin, data) {
      if (plugin != 'ABL_Expert') return;
      if (data.type == 'printer_online') {
        this.printerOnline(data.msg);
      }
      if (data.type == 'state') {
        this.state = data.msg;
        if (this.state != 'idle') {
          this.probedMeshTable(undefined);
          this.fixedMeshTable(undefined);
        }
        this.preheatingEnabled(this.pluginSettings.preheat_selected() != 'Disabled');
        this.preheatingDone(this.state != 'preheating');
        this.onLeveling(this.state != 'idle');
      }
      if (data.type == 'printer_cap') this.updatePrinterCap(JSON.parse(data.msg));
      if (data.type == 'eeprom_mesh') this.eepromMeshTable(this.getMeshTable(JSON.parse(data.msg)));
      if (data.type == 'probed_mesh') {
        this.probedMesh = JSON.parse(data.msg);
        this.probedMeshTable(this.getMeshTable(this.probedMesh));
      }
      if (data.type == 'fixed_mesh') {
        this.probedMeshTable(null);
        this.fixedMeshTable(this.getMeshTable(JSON.parse(data.msg), this.probedMesh));
        if (this.processingDialogVisible) {
          this.processingDialogVisible = false;
          this.processingDialog.modal('hide');
          this.openResultsDialog();
        }
      }
    }
  }, {
    key: 'updatePrinterCap',
    value: function updatePrinterCap(cap) {
      this.printerEeprom(cap.eeprom);
      this.printerAbl(cap.autolevel);
      this.printerZprobe(cap.z_probe);
      this.printerLackCap(!cap.eeprom && !cap.autolevel && !cap.z_probe);
    }
  }, {
    key: 'getMeshTable',
    value: function getMeshTable(mesh, optionalMesh) {
      var meshTable = '<table><tr><th>X/Y index</th>';
      for (var _count = 0; _count < mesh.length; _count++) {
        meshTable += '<th>' + _count + '</th>';
      }
      meshTable += '</tr>';
      var count = 0;
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = mesh[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var meshLine = _step2.value;

          meshTable += '<tr><td>' + count + '</td>';
          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = meshLine[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var meshValue = _step3.value;

              meshTable += '<td>' + meshValue + '</td>';
            }
          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3.return) {
                _iterator3.return();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }

          if (optionalMesh) {
            meshTable += '</tr><tr><td class="opacify">Probe</td>';
            for (var i = 0; i < meshLine.length; i++) {
              meshTable += '<td class="opacify">' + optionalMesh[count][i] + '</td>';
            }
          }
          meshTable += '</tr>';
          count++;
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      meshTable += '</table>';
      return meshTable;
    }
  }, {
    key: 'correctionPreview',
    value: function correctionPreview() {
      xFinalOffset = $('#x_final_offset').val();
      xPointIndex = $('#x_point_index').val();
      xPointOffset = $('#x_point_offset').val();
      console.log('Correction preview: ' + xFinalOffset + '/' + xPointIndex + '/' + xPointOffset);
      this.request('POST', 'correction_preview', [$('#x_final_offset').val(), $('#x_bilinear').is(':checked'), $('#x_point_index').val(), $('#x_point_offset').val(), $('#y_final_offset').val(), $('#y_bilinear').is(':checked'), $('#y_point_index').val(), $('#y_point_offset').val()]);
    }
  }, {
    key: 'doLeveling',
    value: function doLeveling() {
      this.request('POST', 'bed_leveling');
      this.openProcessingDialog();
    }
  }, {
    key: 'cancelLeveling',
    value: function cancelLeveling() {
      if (this.state == 'idle') return;
      this.state = 'idle';
      this.request('POST', 'cancel');
    }
  }, {
    key: 'onProcessingDialogClosed',
    value: function onProcessingDialogClosed() {
      this.processingDialogVisible = false;
      this.cancelLeveling();
    }
  }, {
    key: 'onResultsDialogClosed',
    value: function onResultsDialogClosed() {
      this.resultsDialogVisible = false;
    }
  }, {
    key: 'openProcessingDialog',
    value: function openProcessingDialog() {
      this.processingDialogVisible = true;
      this.tempToolVisible(false);
      this.ruler('');
      if (this.preheatingEnabled()) {
        if (this.temp.tools().length) {
          this.tempToolVisible(true);
        }
      }
      this.processingDialog.modal({ keyboard: false, backdrop: 'static' });
    }
  }, {
    key: 'openResultsDialog',
    value: function openResultsDialog() {
      this.resultsDialogVisible = true;
      this.resultsDialog.modal({ keyboard: false, backdrop: 'static' });
    }
  }, {
    key: 'request',
    value: function request(type, command, args, successCb) {
      var data = function data() {
        if (command && args) return JSON.stringify({ command: command, args: args });
        if (command) return JSON.stringify({ command: command });
      };
      $.ajax({
        url: '/api' + PLUGIN_BASEURL + 'ABL_Expert',
        type: type,
        dataType: 'json',
        data: data(),
        contentType: 'application/json; charset=UTF-8',
        success: function success(data) {
          if (successCb) successCb(data);
        }
      });
    }
  }]);

  return AblExpertViewModel;
}();

OCTOPRINT_VIEWMODELS.push({
  construct: AblExpertViewModel,
  dependencies: ['settingsViewModel', 'controlViewModel', 'temperatureViewModel'],
  elements: ['#processing_dialog_plugin_ABL_Expert', '#results_dialog_plugin_ABL_Expert', '#settings_plugin_ABL_Expert']
});
