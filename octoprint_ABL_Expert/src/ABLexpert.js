class AblExpertViewModel {
  constructor (parameters) {
    this.settingsViewModel = parameters[0]
    this.control = parameters[1]
    this.temp = parameters[2]
    this.preheatOptions = ko.observableArray(undefined)
    this.printerOnline = ko.observable(undefined)
    this.printerEeprom = ko.observable(undefined)
    this.printerAbl = ko.observable(undefined)
    this.printerZprobe = ko.observable(undefined)
    this.printerLackCap = ko.observable(undefined)
    this.preheatingEnabled = ko.observable(false)
    this.preheatingDone = ko.observable(undefined)
    this.eepromMeshTable = ko.observable(undefined)
    this.probedMeshTable = ko.observable(undefined)
    this.fixedMeshTable = ko.observable(undefined)
    this.onLeveling = ko.observable(undefined)
    this.ruler = ko.observable(undefined)
    this.tempToolVisible = ko.observable(false)
    this.probedMesh = []
    this.state = 'idle'
    this.processingDialog = $('#processing_dialog_plugin_ABL_Expert')
    this.processingDialog.modal({keyboard: false, backdrop: 'static', show: false})
    this.processingDialog.on('hidden.bs.modal', () => { this.onProcessingDialogClosed() })
    this.processingDialogVisible = false
    this.resultsDialog = $('#results_dialog_plugin_ABL_Expert')
    this.resultsDialog.modal({keyboard: false, backdrop: 'static', show: false})
    this.resultsDialog.on('hidden.bs.modal', () => { this.onResultsDialogClosed() })
    this.resultsDialogVisible = false
    // Add bed leveling button in control tab with callback
    $("#control-jog-general").append($('#controlAblButton').html())
    this.control.doLeveling = () => { this.doLeveling() }
  }

  onBeforeBinding() {
    this.pluginSettings = this.settingsViewModel.settings.plugins.ABL_Expert
    this.preheatOptions.push('Disabled')
    for (let profile of this.settingsViewModel.settings.temperature.profiles()) {
      this.preheatOptions.push(profile.name())
    }
    this.preheatingEnabled(this.pluginSettings.preheat_selected() != 'Disabled')
    this.preheatingDone(this.state != 'preheating')
    this.onLeveling(this.state != 'idle')
    this.ruler('')
    this.request('GET', null, null, (data) => {
      this.updatePrinterCap(data.printer_cap)
      this.eepromMeshTable(this.getMeshTable(data.eeprom_mesh))
      this.printerOnline(data.printer_online)
    })
  }

  onEventSettingsUpdated(payload) {
    this.onBeforeBinding()
  }

  onAfterBinding() {
    setInterval(() => {
      if (this.processingDialogVisible && this.state != 'idle') {
        if (this.ruler().length >= 3) this.ruler('')
        this.ruler(this.ruler() + '.')
        return
      }
      if (this.state != 'idle') this.openProcessingDialog()
    }, 1000)
  }


  onDataUpdaterPluginMessage(plugin, data) {
    if (plugin != 'ABL_Expert') return
    if (data.type == 'printer_online') {
      this.printerOnline(data.msg)
    }
    if (data.type == 'state') {
      this.state = data.msg
      if (this.state != 'idle') {
        this.probedMeshTable(undefined)
        this.fixedMeshTable(undefined)
      }
      this.preheatingEnabled(this.pluginSettings.preheat_selected() != 'Disabled')
      this.preheatingDone(this.state != 'preheating')
      this.onLeveling(this.state != 'idle')
    }
    if (data.type == 'printer_cap') this.updatePrinterCap(JSON.parse(data.msg))
    if (data.type == 'eeprom_mesh') this.eepromMeshTable(this.getMeshTable(JSON.parse(data.msg)))
    if (data.type == 'probed_mesh') {
      this.probedMesh = JSON.parse(data.msg)
      this.probedMeshTable(this.getMeshTable(this.probedMesh))
    }
    if (data.type == 'fixed_mesh') {
      this.probedMeshTable(null)
      this.fixedMeshTable(this.getMeshTable(JSON.parse(data.msg), this.probedMesh))
      if (this.processingDialogVisible) {
        this.processingDialogVisible = false
        this.processingDialog.modal('hide')
        this.openResultsDialog()
      }
    }
  }

  updatePrinterCap (cap) {
    this.printerEeprom(cap.eeprom)
    this.printerAbl(cap.autolevel)
    this.printerZprobe(cap.z_probe)
    this.printerLackCap(!cap.eeprom && !cap.autolevel && !cap.z_probe)
  }

  getMeshTable(mesh, optionalMesh) {
    let meshTable = '<table><tr><th>X/Y index</th>'
    for (let count=0; count < mesh.length; count++) {
      meshTable += `<th>${count}</th>`
    }
    meshTable += '</tr>'
    let count = 0
    for (let meshLine of mesh) {
      meshTable += `<tr><td>${count}</td>`
      for (let meshValue of meshLine) {
        meshTable += `<td>${meshValue}</td>`
      }
      if (optionalMesh) {
        meshTable += '</tr><tr><td class="opacify">Probe</td>'
        for (let i=0; i<meshLine.length; i++) {
          meshTable += `<td class="opacify">${optionalMesh[count][i]}</td>`
        }
      }
      meshTable += '</tr>'
      count++
    }
    meshTable += '</table>'
    return meshTable
  }

  correctionPreview () {
    xFinalOffset = $('#x_final_offset').val()
    xPointIndex = $('#x_point_index').val()
    xPointOffset = $('#x_point_offset').val()
    console.log(`Correction preview: ${xFinalOffset}/${xPointIndex}/${xPointOffset}`)
    this.request('POST', 'correction_preview', [$('#x_final_offset').val(),
                 $('#x_bilinear').is(':checked'), $('#x_point_index').val(),
                 $('#x_point_offset').val(), $('#y_final_offset').val(),
                 $('#y_bilinear').is(':checked'), $('#y_point_index').val(),
                 $('#y_point_offset').val()])
  }

  doLeveling () {
    this.request('POST', 'bed_leveling')
    this.openProcessingDialog()
  }

  cancelLeveling () {
    if (this.state == 'idle') return
    this.state = 'idle'
    this.request('POST', 'cancel')
  }

  onProcessingDialogClosed () {
    this.processingDialogVisible = false
    this.cancelLeveling()
  }

  onResultsDialogClosed () {
    this.resultsDialogVisible = false
  }

  openProcessingDialog () {
    this.processingDialogVisible = true
    this.tempToolVisible(false)
    this.ruler('')
    if (this.preheatingEnabled()) {
      if (this.temp.tools().length) {
        this.tempToolVisible(true)
      }
    }
    this.processingDialog.modal({keyboard: false, backdrop: 'static'})
  }

  openResultsDialog () {
    this.resultsDialogVisible = true
    this.resultsDialog.modal({keyboard: false, backdrop: 'static'})
  }

  request (type, command, args, successCb) {
    let data = () => {
      if (command && args) return JSON.stringify({command: command, args: args})
      if (command) return JSON.stringify({command: command})
    }
    $.ajax({
      url: `/api${PLUGIN_BASEURL}ABL_Expert`,
      type: type,
      dataType: 'json',
      data: data(),
      contentType: 'application/json; charset=UTF-8',
      success: (data) => { if (successCb) successCb(data) }
    })
  }
}


OCTOPRINT_VIEWMODELS.push({
  construct: AblExpertViewModel,
  dependencies: ['settingsViewModel', 'controlViewModel', 'temperatureViewModel'],
  elements: ['#processing_dialog_plugin_ABL_Expert', '#results_dialog_plugin_ABL_Expert',
             '#settings_plugin_ABL_Expert']
});
