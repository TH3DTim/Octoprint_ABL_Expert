# coding=utf-8
''' Plugin setup file '''

from setuptools import setup
plugin_identifier = 'ABL_Expert'
plugin_package = 'octoprint_ABL_Expert'
plugin_name = 'Auto bed leveling expert'
plugin_version = '0.1'
plugin_description = '''Add features for auto bed leveling and z probe handling
                        on Marlin printers : setting for Z probe offset, preheat 
                        before probing, and mesh probe correction in case of 
                        squeezed axis'''
plugin_author = 'razer'
plugin_author_email = 'razerraz@free.fr'
plugin_url = 'https://framagit.org/razer/Octoprint_ABL_Expert'
plugin_license = 'AGPLv3'
plugin_requires = []
plugin_additional_data = []
plugin_additional_packages = []
plugin_ignored_packages = []
additional_setup_parameters = {}

try:
    import octoprint_setuptools
except ImportError:
    print('Could not import OctoPrint\'s setuptools, are you sure you are ' +
           'running that under the same python installation that OctoPrint ' +
           'is installed under?')
    import sys
    sys.exit(-1)

setup_parameters = octoprint_setuptools.create_plugin_setup_parameters(
    identifier=plugin_identifier,
    package=plugin_package,
    name=plugin_name,
    version=plugin_version,
    description=plugin_description,
    author=plugin_author,
    mail=plugin_author_email,
    url=plugin_url,
    license=plugin_license,
    requires=plugin_requires,
    additional_packages=plugin_additional_packages,
    ignored_packages=plugin_ignored_packages,
    additional_data=plugin_additional_data
)

if additional_setup_parameters:
    # pylint: disable=E0401
    from octoprint.util import dict_merge
    setup_parameters = dict_merge(setup_parameters, additional_setup_parameters)

setup(**setup_parameters)
